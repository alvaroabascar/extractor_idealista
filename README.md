# Extractor Informació Idealista
Eina que permet extreure informació dels habitatges o locals que estan a la venda o per llogar a Idealista i les agències que els gestionen.

Per a més informació sobre com instal·lar i utilitzar l'eina, tenim una entrada al blog d'Investigació Online:
* https://investigacio-online.github.io/posts/idealista/

Aquesta eina forma part del **Projecte Nadki**. Fes una ullada a la nostra pàgina web:
* https://projectenadki.github.io/

# Howto

1. Ve a tu navegador, abre el "Developer Tools" y navega a cualquier página.
2. En el Developer Tools, en el area de Network o similar, escoge cualquier request y copia el Header "User Agent"
3. Lanza el script así:

```
python3 idealista.py --url https://www.idealista.com/venta-viviendas/girona-provincia/ --user-agent 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0'
```